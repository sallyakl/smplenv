<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddIndexToInvoiceItems extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('invoice_items', function (Blueprint $table) {
          $table->index(['invoice_id', 'product_name']);
          $table->foreign('invoice_id')->references('id')->on('invoices')->unsigned()->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('invoice_items', function (Blueprint $table) {
          $table->dropIndex('invoice_items_invoice_id_product_name_index');
          $table->dropForeign('invoices_invoice_id_foreign');
        });
    }
}
