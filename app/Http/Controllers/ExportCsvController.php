<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Invoices;
use Rap2hpoutre\FastExcel\FastExcel;
class ExportCsvController extends Controller
{
    public function export()
    {

      function purchaseProductGenerator() {
        $result = Invoices::select('invoice_num','customers.customer_name','invoice_items.product_name')
                  ->join("invoice_items","invoices.id","invoice_items.invoice_id")
                  ->join("customers","customers.id","invoices.customer_id")
                  ->orderBy('invoices.created_at', 'desc');
        foreach ($result->cursor() as $purchase) {
          yield $purchase;
        }
      }
      (new FastExcel(purchaseProductGenerator()))->export('purchaseproduct.xlsx');
    }
}
